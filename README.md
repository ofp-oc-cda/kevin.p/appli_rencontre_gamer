# Appli_Rencontre_Gamer

```plantuml

left to right direction

actor "User" as user
actor "Admin" as admin

user <|-- admin

usecase "Create account" as CA

usecase "Login" as UC1
usecase "Logout" as UC2
usecase "Edit profil" as UC3
usecase "Manage account" as UC4
usecase "Look at other profiles" as UC5
usecase "Send message" as UC6
usecase "Detailed search" as UC7
usecase "Match" as UC8

usecase "Delete user" as AC1
usecase "Ban user" as AC2
usecase "List all accounts" as AC3
usecase "Create event" as AC4

user --> CA

CA --> UC1
CA --> UC2
CA --> UC3
CA --> UC4
CA --> UC5
CA --> UC6

UC5 --> UC7
UC7 --> UC6
UC6 --> UC8
UC7 --> UC8

admin --> AC1
admin --> AC2
admin --> AC3
admin --> AC4


```

```plantuml

'https://plantuml.com/class-diagram


class Users {
   + id (int)
   + Email (text)
   + Pseudo (varchar)
   + Age (int)
   + Gender (int, relation Genders)
   + LookFor (int, relation Relations)
   + Quest (text)
   + FavoriteGame (int,relation Games)
   + GameOfMoment (int, relation Games)
   + GameType (int, relation GamesType)
   + SomeOfOthersGames (int, relation Games)
   + Plateforme (int,relation Plateformes)
   + HoursPerWeek (int, relation Classes)
   + Talent (int, relation Talents)
   + RegistrationDate (DateTime)
   + Role (int, relation Roles)
}

class Genders{
    + id (int)
    + GendersName (varchar)
}

class Relations{
    + id (int)
    + RelationsName (varchar)
}
 class Games{
    + id (int)
    + GamesName (varchar API?)
    + GamesImage (image)
 }

 class GamesType{
   + id (int)
   + TypeName (varchar)
 }

 class Plateformes{
    + id (int)
    + PlateformesName(varchar)
 }

 class Classes{
    + id (int)
    + ClassesName (varchar)
    + Logo (image)
 }

 class Talents{
    + id (int)
    + TalentsName (varchar)
    + TalentsLogo (image)
 }

 class Roles{
    + id (int)
    + RolesName (varchar)
}

class Messages{
 + id (int)
 + UserID1 (int, relation Users)
 + UserID2 (int, relation Users)
 + MessageDate (DateTime)
 + Content (longtext)
}

class Matching{
   + id (int)
   + UserID1 (int, relation Users)
   + UserID2 (int, relation Users)
   + isMatching (bool)
}

class Events{
   + id (int)
   + EventsName (varchar API?)
   + Description (text)
   + isParticipating (bool = null)
   + UsersID (int, relation Users)
}

Users *-- Genders
Users *-- Relations
Users *-- Games
Users *-- Plateformes
Users *-- Classes
Users *-- Talents
Users *-- Roles
Users *-- GamesType

Users --* Messages
Users --* Matching
Users --* Events
